#!/bin/bash
GZIP=-9  tar -czvf ~/daily_backup_`date +%d-%m_%Y`.tar.gz \
--exclude='BlackmagicDesign' \
--exclude='Image-Line' \
--exclude='Experda' \
--exclude='rundeck' \
--exclude='Postman'  \
~/scripts \
~/Documents  \
~/Dev \
~/.p10k.zsh  \
~/.zshrc \
~/.aliases \
~/.davmail.properties \
~/.vimrc  \
~/.bashrc \
~/.gitconfig
